
// Vier Gewinnt - JS

// MAIN FUNCTION
$(document).ready(function() {

  // Variables, Arrays and Objects
  var hoehe = 6; // 6
  var breite = 7; // 7
  var cur_pl = {p_id: 1};
  var spielfeld_array = new Array();
  var warning = {w: false};

  /*var default_player1 = {name: "Player 1", p_id: 1, color: 'red'};
  var default_player2 = {name: "Player 2", p_id: 2, color: 'blue'};
  var cur_p1 = default_player1;
  var cur_p2 = default_player2;
  var cur_player = cur_p1;*/

  spielfeldArrayErstellen(breite, hoehe, spielfeld_array);
  spielfeldErstellen(breite, hoehe);

  /* for testing
  var test = document.createElement('tr');
  var inside = document.createTextNode('test');
  test.appendChild(inside);
  $('#spielanzeigen-table').append(test);
  */

  $('#restart-btn').on('click', function() {
    spielfeldReseten(breite, hoehe);

    spielfeldArrayReseten(breite, hoehe, spielfeld_array);
  });

  $('.einwurf-btn').on('click', function() {
    $(this).clearQueue().css('opacity', '0.5');
    $(this).fadeTo('fast', 1);
    muenzeEinwerfen(breite, hoehe, spielfeld_array, cur_pl, this.id, warning);
  });

  $('#option-btn').on('click', function() {
    window.close();// not wortking if not self opened by script
  });

});


//----------------------------------------------------------------------------//
//                            FUNCTIONS
//----------------------------------------------------------------------------//

function spielfeldArrayErstellen(breite, hoehe, spielfeld_array) {
  for (var i = 0; i < breite; i++) {
    spielfeld_array[i] = new Array();
    for (var k = 0; k < hoehe; k++) {
      spielfeld_array[i][k] = 0;
    }
  }
}
//----------------------------------------------------------------------------//

function spielfeldArrayReseten(breite, hoehe, spielfeld_array) {
  for (var i = 0; i < breite; i++) {
    for (var k = 0; k < hoehe; k++) {
      spielfeld_array[i][k] = 0;
    }
  }
}

//----------------------------------------------------------------------------//

function spielfeldErstellen(breite, hoehe) {
  // zuerst das Tablelle-"Grundgerüst" erstellen
  var tbl = document.createElement('table');
  var btn_row = document.createElement('tr');
  btn_row.id = 'btn-row';
  tbl.id = 'spielanzeigen-table';
  tbl.appendChild(btn_row);
  document.getElementById('spielfeld').appendChild(tbl);

  // buttons in ersten(obersten) reihe erstellen
  for (var i = 0; i < breite; i++) {
    var btn = document.createElement('th');
    var btn_inside = document.createElement('i');
    btn_inside.className = 'fa fa-chevron-down fa-2x';
    btn.appendChild(btn_inside);
    btn.className = 'einwurf-btn';
    btn.id = 'btn' + i;

    document.getElementById('btn-row').appendChild(btn);
  }

  /* spielfeld kacheln erstellen
   *
   * zeilen = i   - rows    -> hoehe
   * spalten = k  - columns -> breite
   */
  for (var i = 0; i < hoehe; i++) {
    var neue_zeile = document.createElement('tr');
    neue_zeile.id = 'row' + i;

    for (var k = 0; k < breite; k++) {
      var feld = document.createElement('td');
      //var feld_inside = document.createElement('div');
      //feld_inside.className = 'leeres-feld';
      feld.className = 'leeres-feld';
      feld.id = 'feld[' + k + '][' + i + ']';

      neue_zeile.appendChild(feld);
      document.getElementById('spielanzeigen-table').appendChild(neue_zeile);
    }
  }
}
//----------------------------------------------------------------------------//

function spielfeldReseten(breite, hoehe) {
  //$('#spielanzeigen-table').remove();
  //spielfeldErstellen(breite, hoehe);
  $('.p1').remove();
  $('.p2').remove();
}
//----------------------------------------------------------------------------//

function muenzeEinwerfen(breite, hoehe, spielfeld_array, cur_pl, btn_id, warning) {
  var id_num = parseInt(btn_id[3]); // = breite bzw. x-Wert
  //alert('Number: ' + id_num);
  var muenze_eingeworfen = false;

  for (var i = hoehe-1; i >= 0; i--) {
    if (spielfeld_array[id_num][i] === 0) {
      spielfeld_array[id_num][i] = cur_pl.p_id;
      var muenze = document.createElement('div');
      muenze.className = 'p' + cur_pl.p_id;
      document.getElementById('feld[' + id_num + '][' + i + ']').appendChild(muenze);

      muenze_eingeworfen = true;
      break;
    }
  }

  if (muenze_eingeworfen === true) {
    // Spieler wechseln
    if (cur_pl.p_id === 1) {
      cur_pl.p_id = 2;
    } else {
      cur_pl.p_id = 1;
    }
    if (warning.w) {
      $('spalte-voll').fadeTo('slow', 0);
      $('#warning').toggle();
      $('#spalte-voll').remove();
    }
    warning.w = false;
  } else if (!warning.w) {
    warning.w = true;
    var warn = document.createElement('div');
    var warn_p = document.createElement('p');
    var w_p_inside = document.createTextNode('Bitte eine andere Spalte wählen');
    warn.id = 'spalte-voll';
    warn_p.appendChild(w_p_inside);
    warn_p.id = 'spalte-voll-text';
    warn.appendChild(warn_p);
    document.getElementById('warning').appendChild(warn);

    $('#warning').toggle();
    //alert("Please select another column");
  } else {
    $('#spalte-voll-text').clearQueue().css('opacity', '0');
    $('#spalte-voll-text').fadeTo('slow', 1);
  }
}
//----------------------------------------------------------------------------//

function trottleMode(array) {
  for (var i = 0; i < array.length; i++) {
    for (var k = 0; k < array[0].length; k++) {
      array[i];
    }
  }
}
